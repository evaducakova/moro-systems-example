package com.morosystems.repository;

import org.springframework.data.repository.CrudRepository;

import com.morosystems.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

}