package com.morosystems.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "User does not exist")
public class UserDoesNotExistsException extends RuntimeException {

	public UserDoesNotExistsException(String message) {
        super(message);
    }
} 
