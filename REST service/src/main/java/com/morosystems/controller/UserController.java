package com.morosystems.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.morosystems.model.User;
import com.morosystems.repository.UserRepository;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	UserRepository repository;

	/**
	 * CREATE
	 * 
	 * @param json
	 *            representation of user.
	 * @return created user
	 */
	@PostMapping(value = "/users")
	public ResponseEntity<String> postUser(@Valid @RequestBody User user) {
		repository.save(new User(user.getName(), user.getUsername(), user.getPassword()));

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<>(responseHeaders, HttpStatus.CREATED);
	}

	/**
	 * GET all users
	 * 
	 * @return list of all users
	 */
	@GetMapping(value = "/users")
	public List<User> getAll() {
		List<User> list = new ArrayList<>();
		Iterable<User> users = repository.findAll();

		users.forEach(list::add);
		return list;
	}

	/**
	 * READ
	 * 
	 * @param id
	 *            of searched user
	 * @return found user
	 */
	@GetMapping(value = "/users/{id}")
	public User getUserById(@PathVariable long id) {
		if (!repository.exists(id)) {
			throw new UserDoesNotExistsException("User does not exist");

		}
		return repository.findOne(id);
	}

	/**
	 * UPDATE
	 * 
	 * @param json
	 *            representation of user to update
	 * @return saved user
	 */
	@PutMapping(value = "/users")
	public ResponseEntity<String> updateUser(@Valid @RequestBody User user) {
		repository.save(user);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
	}

	/**
	 * DELETE
	 * 
	 * @param id
	 *            of user
	 * @return deleted user
	 */
	@DeleteMapping(value = "/users/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable long id) {
		if (!repository.exists(id)) {
			throw new UserDoesNotExistsException("User does not exist");
		}

		repository.delete(id);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
	}
}
