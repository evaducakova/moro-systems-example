package com.morosystems.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.BeforeClass;
import org.junit.Test;

public class UserTest {

	private static Validator validator;
	private static final String[] LANGUAGE_VAR_NULL = {"may not be null", "nemôže byť null"};
	private static final String[] LANGUAGE_VAR_LENGTH = {"size must be between 2 and 30", "veľkosť musí byť medzi 2 a 30"};

	@BeforeClass
	public static void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void nameIsNotNull() {
		User user = new User(null);

		Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

		assertEquals(1, constraintViolations.size());
		assertTrue(LANGUAGE_VAR_NULL[0].equals(constraintViolations.iterator().next().getMessage()) 
				|| LANGUAGE_VAR_NULL[1].equals(constraintViolations.iterator().next().getMessage()));
	}

	@Test
	public void nameIsTooShort() {
		User user = new User("N");

		Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);

		assertEquals(1, constraintViolations.size());
		assertTrue(LANGUAGE_VAR_LENGTH[0].equals(constraintViolations.iterator().next().getMessage()) 
				|| LANGUAGE_VAR_LENGTH[1].equals(constraintViolations.iterator().next().getMessage()));
	}

	@Test
	public void nameIsValid() {
		User user = new User("Darth Vader");

		Set<ConstraintViolation<User>> constraintViolations = validator.validate(user);
		assertEquals(0, constraintViolations.size());
	}
}
