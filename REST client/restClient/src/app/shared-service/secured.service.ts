import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

@Injectable()
export class SecuredService {

  constructor(private http: Http) {
  }

  deleteUser(id: Number) {
    let headers = new Headers({'Content-Type': 'application/json'});
    headers.append('Authorization', 'Basic ' + btoa('admin:admin'));
    let options = new RequestOptions({headers: headers});
    return this.http.delete('http://localhost:8080/secured/deleteUser/' + id, options);
  }

}
