import { TestBed, inject } from '@angular/core/testing';

import { SecuredService } from './secured.service';

describe('SecuredService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecuredService]
    });
  });

  it('should be created', inject([SecuredService], (service: SecuredService) => {
    expect(service).toBeTruthy();
  }));
});
