import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {UserComponent} from './components/user/user.component';
import {UserService} from './shared-service/user.service';
import {SecuredService} from './shared-service/secured.service';
import {NavigationComponent} from './components/navigation/navigation.component';
import {SecuredComponent} from './components/secured/secured.component';

const appRoutes: Routes = [
  {path: '', component: UserComponent},
  {path: 'Authentication', component: SecuredComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    NavigationComponent,
    SecuredComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [UserService, SecuredService],
  bootstrap: [AppComponent]
})
export class AppModule {}
