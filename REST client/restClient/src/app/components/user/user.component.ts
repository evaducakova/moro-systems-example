import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {clone} from 'lodash';

import {UserService} from '../../shared-service/user.service';
import {User} from '../../user';

@Component({
  selector: 'app-listuser',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  private users: User[];
  userForm = false;
  isNewUser: boolean;
  newUser: any = {};
  editUserForm = false;
  editedUser: any = {};

  constructor(private _userService: UserService, private _router: Router) {}

  ngOnInit() {
    this._userService.getUsers().subscribe((users) => {
      console.log(users);
      this.users = users;
    }, (error) => {
      console.log(error);
    });
  }

  showEditUserForm(user: User) {
    if (!user) {
      this.userForm = false;
      return;
    }
    this.editUserForm = true;
    this.editedUser = clone(user);
  }

  showAddUserForm() {
    if (this.users.length) {
      this.newUser = {};
    }
    this.userForm = true;
    this.isNewUser = true;
  }

  saveUser() {
    if (this.isNewUser) {
      this._userService.createUser(this.newUser).subscribe((newUser) => {
        console.log(newUser);
      }, (error) => {
        console.log(error);
      });
      this.userForm = false;
      location.reload();
    }
  }

  updateUser() {
    this._userService.updateUser(this.editedUser).subscribe((editedUser) => {
      console.log(editedUser);
    }, (error) => {
      console.log(error);
    });
    this.editUserForm = false;
    this.editedUser = {};
    location.reload();
  }

  deleteUser(deletedUser) {
    this._userService.deleteUser(deletedUser.id).subscribe((deletedUser) => {
      console.log(deletedUser);
    }, (error) => {
      console.log(error);
    });
    location.reload();
  }

  cancelEdits() {
    this.editedUser = {};
    this.editUserForm = false;
  }

  cancelNewUser() {
    this.newUser = {};
    this.userForm = false;
  }
}
