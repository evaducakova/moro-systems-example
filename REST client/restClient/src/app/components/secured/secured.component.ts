import {Component, OnInit} from '@angular/core';

import {User} from '../../user';
import {UserService} from '../../shared-service/user.service';
import {SecuredService} from '../../shared-service/secured.service';

@Component({
  selector: 'app-secured',
  templateUrl: './secured.component.html',
  styleUrls: ['./secured.component.css']
})
export class SecuredComponent implements OnInit {

  private users: User[];

  constructor(private userService: UserService, private securedService: SecuredService) {}

  ngOnInit() {
    this.userService.getUsers().subscribe((users) => {
      console.log(users);
      this.users = users;
    }, (error) => {
      console.log(error);
    });
  }

  deleteUser(deletedUser) {
    this.securedService.deleteUser(deletedUser.id).subscribe(response => {
      console.log(response);
    }, (error) => {
      console.log(error);
    });
    location.reload();
  }

}
